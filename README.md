# Cnt

## Demonstration

This program helps you track things you're doing. I say "things"
because you give it the count for the day (or week or whatever).
Example:

```
cnt "$(wc -w ~/stories/new-story/files/*)" -d2020-03-23 -g5000
```

Say 2020-03-20 is in three days. cnt might spit out:

```
interval total
?/565.62 3362/5000
```

It's telling you you need to do 565.62 words per day to hit your goal.
The question mark is how many you've done during this "interval".

An interval is a duration. By default it's days. You can specify an
arbitrary duration in a string that looks like this: "2d3w8y." That's
a digit followed by "s" (seconds), "m" (minutes), "h" (hours), "d"
(days), "M" months or "y" (years).

The question mark is there because without a passing a "tracker file"
there's no way for the program to know how much you've done this
interval. With a tracker file passed, the command is now like
this:

```
cnt "$(wc -w ~/stories/new-story/files/*)" -d2020-03-23 -g5000 \
	-fwords.txt
```

Now you will get

```
interval total
0/565.62 3362/5000
```

Say you write 100 words. Now cnt will tell you:

```
interval   total
100/565.62 3462/5000
```

The 3462 is the number of words you've done in total, and the 5000 is
the goal you've set yourself.

You might think it's weird that the program wants you to get the
number for it rather than doing it for you in some way. The reason is
is this makes it potentially useful for all sorts of things. I'm using
it to track progress in my stories and also to track how many stories
I've done this year. I'm aiming for 52. cnt's telling me I have to do
0.99 per week to make it.

## How to use it, say I

Put a shell script in your working directory that looks like:

```
#!/bin/zsh

words -d2020-03-21T23:59:59 -g5000 -fwords.txt "$(wc -w story.md)" $@
```

Don't forget the $@, so you can pass other flags, if you like.

## Why there's no configuration file

Maybe you, figment, are thinking a tracker program should take
configuration file. "There's three _manditory_ options, man!"

Firstly, I hate when people talk use emphasis to humanise their
stark, empty, frightening online text.

Secondly, I don't think so. What's the difference between the
example script up there and:

```
files = "~/stories/new-story/files/*"
deadline = 2020-03-21T23:59:59
goal = 5000
```
?

??????????????

## Other options

There's --log, which prints how much you've done each interval, like:

```
20-03-11T00-00-00 1823
20-03-13T00-00-00 0
20-03-14T00-00-00 1132
20-03-16T00-00-00 65
20-03-17T00-00-00 0
20-03-19T00-00-00 342
20-03-20T00-00-00 3362
```

There's --remaining, which prints what you're still to do:

```
interval      total
873.59,873.59 1638,5000
```

You've got all of your words to write today and 1638 of them to go in
total.

--progress-today and --progress-total will give you daily or total
progress as a percentage.

## Other things

By default, numbers above 100 will be rounded. You can change the
point after which rounding takes place with --round-threshold.
--round-threshold=-1 means never round.
