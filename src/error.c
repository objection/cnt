#define _GNU_SOURCE
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

void usage (void) {
	fprintf (stderr, "usage: %s [OPTION ...] COUNT\n", program_invocation_name);
	exit (1);
}


void cnt_err (char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	char *buf = NULL;
	vasprintf (&buf, fmt, ap);
	fprintf (stderr, "%s: %s", program_invocation_name, buf);
	if (errno) fprintf (stderr, ": %s\n", strerror (errno));
	else fprintf (stderr, "\n");
	free (buf);
	exit (1);
}

