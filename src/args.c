#include "config.h"
#include "args.h"
#include "error.h"
#include "util.h"
#include <argp.h>
#include <assert.h>

struct args args;

// So I can access it in parse_opt and not add it to struct args. I
// should have left it global.
struct ts_time *global_now;

char *time_unit_str;

enum longopts {
	LONGOPTS_NICE_LIE = 500,
};

const char *argp_program_version = "cnt 1.2";

enum GOT {
	GOT_THRESHOLD = 1 << 0,
	GOT_COUNT = 1 << 1,
	GOT_START = 1 << 2,
	GOT_INTERVAL = 1 << 3,
} got;

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
	switch (key) {
		case LONGOPTS_NICE_LIE:
			args.flags |= ARGS_FLAG_NICE_LIE;
			break;
#if 0
		case 'b':
			args.flags |= ARGS_FLAG_BACKWARDS;
			break;
#endif
		case 'd':
			args._deadline = arg;
			break;
		case 'f':
			args.tracker_file = arg;
			break;
		case 'g':
			get_double (&args.goal, arg);
			break;
		case 'i':
			time_unit_str = arg;
			break;
		case 'l':
			args.flags |= ARGS_FLAG_PRINT_LOG;
			break;
		case 'L':
			args.flags |= ARGS_FLAG_LIE;
			break;
		case 'p':
			args.flags |= ARGS_FLAG_PRINT_DAILY_PERCENT;
			break;
		case 'P':
			args.flags |= ARGS_FLAG_PRINT_TOTAL_PERCENT;
			break;
		case 'r':
			args.flags |= ARGS_FLAG_REMAINING;
			break;
		case 's':
			args.start_time = ts_time_from_string (arg, NULL,
					&(*global_now).timestamp,
					&(*global_now).tm, 0);
			if (ts_errno)
				cnt_err ("couldn't get time from \"%s\": %s\n",
						arg, ts_strerror (ts_errno));
			if (args.start_time.timestamp >= (*global_now).timestamp)
				cnt_err ("you can't set --start to greater or equal to now");
			got |= GOT_START;
			break;
		case 'S':
			get_double (&args.start_val,  arg);
			break;
		case 't':
			args.flags |= ARGS_FLAG_TOUGH_LOVE;
			break;
		case 'T':
			if (get_int (&args.round_threshold, arg))
				cnt_err ("\"%s\" is bad integer", arg);
			got |= GOT_THRESHOLD;
			break;
		case ARGP_KEY_ARG:
			if (got & GOT_COUNT)
				cnt_err ("Only one COUNT allowed");
			if (get_double (&args.count, arg))
				cnt_err ("\"%s\" is bad float", arg);
			got |= GOT_COUNT;
			break;
		default:
			break;
	}
	return 0;
}
#undef CHECK_NDAYS

static void get_imprecise_time_interval (struct ts_duration *res,
		enum imprecise_time_unit *res_2, char
		*time_unit_str) {
	if (0 == strcmp ("1m", time_unit_str)
			|| 0 == strncmp ("minutely", time_unit_str, 6)) {
		*res = ts_duration_from_duration_string ("1m", NULL);
		*res_2 = IMPRECISE_TIME_UNIT_MINUTELY;
	} else if (0 == strcmp ("1h", time_unit_str)
			|| 0 == strncmp ("hourly", time_unit_str, 4)) {
		*res = ts_duration_from_duration_string ("1h", NULL);
		*res_2 = IMPRECISE_TIME_UNIT_HOURLY;
	} else if (0 == strcmp ("1d", time_unit_str)
			|| 0 == strcmp ("daily", time_unit_str)
			|| 0 == strcmp ("day", time_unit_str)) {
		*res = ts_duration_from_duration_string ("1d", NULL);
		*res_2 = IMPRECISE_TIME_UNIT_DAILY;
	} else if (0 == strcmp ("1w", time_unit_str)
			|| 0 == strncmp ("weekly", time_unit_str, 4)) {
		*res = ts_duration_from_duration_string ("1w", NULL);
		*res_2 = IMPRECISE_TIME_UNIT_WEEKLY;
	} else if (0 == strcmp ("1M", time_unit_str)
			|| 0 == strncmp ("monthly", time_unit_str, 5)) {
		*res = ts_duration_from_duration_string ("1M", NULL);
		*res_2 = IMPRECISE_TIME_UNIT_MONTHLY;
	} else if (0 == strcmp ("1y", time_unit_str)
			|| 0 == strncmp ("yearly", time_unit_str, 4)) {
		*res = ts_duration_from_duration_string ("1y", NULL);
		*res_2 = IMPRECISE_TIME_UNIT_YEARLY;
	} else {
		cnt_err ("\
if no tracker file, --interval must be \"minutely\", \"hourly\", \"daily\"\n\
\"monthly\" or \"yearly\"");
	}
}

static struct ts_time get_this_interval_start (struct ts_time *now,
		enum imprecise_time_unit imprecise_time_unit) {
	struct ts_time res = {0};
	if (got & GOT_START) {
		res = args.start_time;
		ts_add_dur_to_time_until (&res, (*now).timestamp,
				&args.interval_duration, 1);
	} else {
		struct tm set_to_tm = {
			.tm_sec = 0,
			.tm_min = imprecise_time_unit >=
				IMPRECISE_TIME_UNIT_MINUTELY? 0: -1,
			.tm_hour = imprecise_time_unit >=
				IMPRECISE_TIME_UNIT_HOURLY? 0: -1,
			.tm_mday = imprecise_time_unit >=
				IMPRECISE_TIME_UNIT_DAILY? 0: -1,
			.tm_wday = imprecise_time_unit >=
				IMPRECISE_TIME_UNIT_WEEKLY? 0: -1,
			.tm_mon = imprecise_time_unit >=
				IMPRECISE_TIME_UNIT_MONTHLY? 0: -1,
			.tm_year = imprecise_time_unit >=
				IMPRECISE_TIME_UNIT_YEARLY? 0: -1,
		};
		res = ts_time_with_units_set ((*now).timestamp,
				set_to_tm);
	}
	return res;
}

void get_args (int argc, char **argv, struct ts_time now) {

	global_now = &now;
	args = (struct args) {
		.n_time_units = -1,
	};

	struct argp_option options[] = {
#if  0
		{"backwards", 'b', NULL, 0, "\
If your goal is lower than what you started with. See end"},
#endif
		{"deadline", 'd', "TIME STRING", 0, "The deadline"},
		{"goal", 'g', "DOUBLE", 0, "How many THINGS you're aiming to do"},
		{"interval", 'i', "DURATION", 0,
			"How long the interval is"},
		{"lie", 'L', NULL, 0,
			"Pretend you have to do 10\% more today than you really do"},
		{"logfile", 'f', "PATH", 0,
			"The (optional) file in which to track totals"},
		{"round-threshold", 'T', "INTEGER", 0,
			"The number after which to round results. Pass -1 to never round"},
		{"log", 'l', NULL, 0, "Print the date and count of each logfile \
entry. Still writes to the logfile, if provided"},
		{"nice-lie", LONGOPTS_NICE_LIE, NULL, 0,
			"Pretend you have to do 10\% less today than you really do"},
		{"progress-today", 'p', NULL, 0,
			"Your progress today in percent of today's goal. The \% is \
ommitted"},
		{"progress-total", 'P', NULL, 0,
			"Your progress in percent of total goal. The \% is ommitted"},
		{"remaining", 'r', NULL, 0,
			"Print THINGS remaining rather than done. See end for more"},
		{"start-date", 's', "ISO TIME STRING", 0,
			"Specify a start date/time. See end"},
		{"start-val", 'S', "DOUBLE", 0,
			"Specify a start date/time. See end"},
		{"tough-love", 't', NULL, 0,
			"Print \"You lose\" if you miss your deadline"},
		{0},
	};

	struct argp argp = {
		options, parse_opt, "COUNT",
		"Figure out how many THINGS you have to do this INTERVAL to achieve \
your GOAL before the DEADLINE\v"
			"\
Use the program like (eg) this: 'cnt --goal=5000 \
--deadline=2020-03-18 \"$(wc -w ~/stories/new-story/files/*)\"'. The wc \
command is your count. You tell the program how much you've done. Since \
you'll want to \
keep checking your progress, put the command in a shell script. You might \
think the need to doing this might make me give this program a configuration \
file, but I think that would make the program more complicated rather than \
less.\n\
\n\
If you want to track how much you've done this --interval (by default this \
is a day), add a tracker file to your command with -f, eg \"-f words.txt\" \
\n\
A --deadline of a date only without the time (eg 2020-03-18) is one that \
ends at 00:00:00.\n\
\n\
If you don't specify --start-date, your --intervals can only be a single minute, \
hour, day, week, month or year. And the start of the current interval \
is the start of that hour, day, week, month or year. You'll only need this \
if you're starting on eg Tuesday and finishing on the following Tuesday.\n\
\n\
With --remaining, if you've done more than your goal, 0 will be printed \
instead of a minus number. Eg, your goal is 2 and you've done 3, you'll \
get 0,2 instead of -1,3.\n"
	};

	argp_parse (&argp, argc, argv, 0, NULL, &args);

	if (!(got & GOT_COUNT)) usage ();

	enum imprecise_time_unit imprecise_time_unit = 0;
	if (time_unit_str) {
		if (got & GOT_START)
			args.interval_duration = ts_duration_from_duration_string
				(time_unit_str, NULL);
		else
			get_imprecise_time_interval (&args.interval_duration,
					&imprecise_time_unit, time_unit_str);
		got |= GOT_INTERVAL;
	}

	if (!(got & GOT_INTERVAL)) {
		args.interval_duration = ts_duration_from_duration_string ("1d", NULL);
		imprecise_time_unit = IMPRECISE_TIME_UNIT_DAILY;
	}

	args._this_interval_start = get_this_interval_start (&now,
			imprecise_time_unit);

	// Couldn't have done this in parse_opt because its answer depends on
	// whethere ARGS_FLAG_END_IS_BEGINNING_OF_DAY is set.

	if (args._deadline) {
		time_t deadline_timestamp = ts_timestamp_from_string (args._deadline,
				NULL, &now.timestamp, &now.tm, 0);
		/* if (args.flags & ARGS_FLAG_END_IS_BEGINNING_OF_DAY) { */
		/* 	struct tm set_to = { */
		/* 		.tm_mon = -1, .tm_year = -1, .tm_mday = -1, */
		/* 		.tm_hour = 0, .tm_min = 0, .tm_sec = 0, */
		/* 	}; */
		/* 	deadline_timestamp = ts_timestamp_with_units_set */
		/* 		(deadline_timestamp + (!(args.flags & ARGS_FLAG_END_IS_BEGINNING_OF_DAY)? */
		/* 						TS_SECS_FROM_DAYS (1): 0), set_to); */
		/* } */
		if (deadline_timestamp <= now.timestamp) {
			if (args.flags & ARGS_FLAG_TOUGH_LOVE)
				cnt_err ("You lose");
			else
				cnt_err ("Deadline needs to be after now");
		}
		time_t secs_till_deadline = deadline_timestamp - now.timestamp;

		// Get the number of, eg, days until the deadline.
		args.n_time_units = (double) (secs_till_deadline /
			 (double) ts_secs_from_duration (&args.interval_duration, &now.tm));

		// If there's more than one, eg, day until the deadline, lose
		// the fractional part. If you don't, your daily goal will
		// rise throughout the day.
		if (args.n_time_units > 1)
			args.n_time_units = (int) args.n_time_units;
	}

	if ((args.flags & ARGS_FLAG_LIE) && (args.flags & ARGS_FLAG_NICE_LIE))
		cnt_err ("you can't do --lie and --nice-lie together");

	if (!(args.flags & ARGS_FLAG_PRINT_LOG) && args.n_time_units == -1)
		cnt_err ("you need to provide --deadline");

	// Should check here to see if the time you've entered is valid
	/* if (!(args.flags & ARGS_FLAG_END_IS_BEGINNING_OF_DAY)) */
	/* 	args.n_time_units += 1; */

	if (!(args.flags & ARGS_FLAG_PRINT_LOG) && !args.goal)
		cnt_err ("you need to provide --goal");
	if (!(got & GOT_THRESHOLD)) args.round_threshold = DEFAULT_ROUND_THRESHOLD;

}

