#include "../lib/time_string/time_string.h"
#include "../lib/darr/darr.h"
#include <stdio.h>

struct interval {
	struct ts_time date;
	double total;
	double today;
};

DECLARE_ARR (struct interval, interval);

enum path_or_fp_type {
	POFP_PATH,
	POFP_FP,
};
struct path_or_fp {
	union {
		char *path;
		FILE *fp;
	};
	enum path_or_fp_type type;
};

struct interval_arr read_tracker_file (char *file, struct ts_time *now); 
void write_to_tracker_file (struct path_or_fp file,
		struct interval_arr intervals); 

