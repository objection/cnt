
enum args_flags {
	ARGS_FLAG_PRINT_LOG = 1 << 0,
	ARGS_FLAG_REMAINING = 1 << 1,
	ARGS_FLAG_LIE = 1 << 2,
	ARGS_FLAG_NICE_LIE = 1 << 3,
	ARGS_FLAG_PRINT_DAILY_PERCENT = 1 << 4,
	ARGS_FLAG_PRINT_TOTAL_PERCENT = 1 << 5,
	ARGS_FLAG_TOUGH_LOVE = 1 << 6,
	/* ARGS_FLAG_BACKWARDS = 1 << 7, */
};

enum imprecise_time_unit {
	IMPRECISE_TIME_UNIT_MINUTELY,
	IMPRECISE_TIME_UNIT_HOURLY,
	IMPRECISE_TIME_UNIT_DAILY,
	IMPRECISE_TIME_UNIT_WEEKLY,
	IMPRECISE_TIME_UNIT_MONTHLY,
	IMPRECISE_TIME_UNIT_YEARLY,
};

struct args {
	enum args_flags flags;
	int round_threshold;
	char *_deadline;
	double n_time_units;
	double goal;
	char *tracker_file;
	double count;
	double start_val;
	struct ts_time start_time;
	struct ts_duration interval_duration;
	struct ts_time _this_interval_start;
};
extern struct args args;

void get_args (int argc, char **argv, struct ts_time now);
