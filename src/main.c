#define _GNU_SOURCE
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <errno.h>
#include <math.h>
#include "../config.h"
#include "args.h"
#include "error.h"
#include "util.h"
#include "tracker.h"
#include "../lib/wc/wc.h"
#include "../lib/slurp/slurp.h"
#include "../lib/print-table/print-table.h"

char scratch[MAX_TOKEN_LEN];

static void *xmalloc (int n_bytes) {
	void *res = malloc (n_bytes);
	if (!res) assert (!"malloc failed");
	return res;
}

#define PREC(val) \
	(args.round_threshold != -1? \
	 ((val >= args.round_threshold || val == (int) val)? \
	 0: DOUBLE_PRINT_PRECISION): val == (int) val? 0: DOUBLE_PRINT_PRECISION)

#define ROUND(val) \
	(args.round_threshold == -1? val: val >= args.round_threshold? \
	 round (val): val)

void print_log (struct interval_arr intervals) {

	for (struct interval *interval = intervals.d;
			interval - intervals.d < intervals.n; interval++) {
		ts_string_from_tm (scratch, MAX_TOKEN_LEN, &(*interval).date.tm,
				"%y-%m-%dT%H-%M-%S");
		fprintf (stdout, "%s %.*f\n", scratch,
				PREC ((*interval).today),
				ROUND ((*interval).today));
	}
}

// Stoken from https://www.codevscolor.com/c-find-total-digits-number/
int n_digits (int num) {
	if (num == 0) return 1;
	int res = 0;
	while (num){
		num = num / 10;
		res++;
	}
	return res;
}

// No checking num_2 is > 0. Checks num_2 is because if it is, there's
// a - in front of it.
// Assumes the format is dddd<single char>dddd
int get_first_token_len (int num_1, int num_2) {
	int n_num_1 = n_digits (num_1);
	int n_num_2 = n_digits (num_2);
	if (num_1 < 0) n_num_1 += 1;
	return n_num_1 + n_num_2 + 1;
}

// Leave this here for now in case I have problems
#if 0
int prec (double val, int round_threshold) {
	if (round_threshold != -1) {
		if (val >= round_threshold || val == (int) val)
			return 0;
		else return DOUBLE_PRINT_PRECISION;
	} else {
		if (val == (int) val)
			return 0;
		else return DOUBLE_PRINT_PRECISION;
	}
}
#endif

// This function's to make it easy to use cnt in a script. That's what
// I'm doing, sending a nag notification if daily percent < 100%.
static void print_daily_percent (double today, double daily_goal) {
	double percent = today / daily_goal * 100.0;
	printf ("%d\n", (int) percent);
}

// This function's to make it easy to use cnt in a script. That's what
// I'm doing, sending a nag notification if daily percent < 100%.
static void print_total_percent (double total, double goal) {
	double percent = (double) total / (double) goal * 100.0;
	printf ("%d\n", (int) percent);
}

static void print (double today, // Ignored if !args.tracker_file
		double daily_goal) {

	struct record records[2];
	records[0].fields = xmalloc (sizeof *records[0].fields * 2);
	records[1].fields = xmalloc (sizeof *records[0].fields * 2);
	records[0].fields[0] = "interval";
	records[0].fields[1] = "total";

	daily_goal = args.flags & ARGS_FLAG_LIE?
		daily_goal * LIE_SCALE:
		args.flags & ARGS_FLAG_NICE_LIE?
		daily_goal * NICE_LIE_SCALE: daily_goal;


	if (args.flags & ARGS_FLAG_REMAINING) {
		if (!args.tracker_file) {
			double total_remaining = args.goal - args.count > 0?
				args.goal - args.count: 0;
			asprintf (&records[1].fields[0], "?,%.*f",
					PREC (daily_goal), ROUND (daily_goal));
			asprintf (&records[1].fields[1], "%.*f,%.*f",
					PREC (total_remaining), ROUND (total_remaining),
					PREC (args.goal), ROUND (args.goal));
		} else {
			double remaining = daily_goal - today > 0? daily_goal: 0;
			double total_remaining = args.goal - args.count > 0?
				args.goal - args.count: 0;
			asprintf (&records[1].fields[0], "%.*f,%.*f",
					PREC (remaining), ROUND (remaining),
					PREC (daily_goal), ROUND (daily_goal));
			asprintf (&records[1].fields[1], "%.*f,%.*f",
					PREC (total_remaining), ROUND (total_remaining),
					PREC (args.goal), ROUND (args.goal));
		}
	} else {
		if (!args.tracker_file) {
			asprintf (&records[1].fields[0], "?/%.*f",
					PREC (daily_goal), ROUND (daily_goal));
			asprintf (&records[1].fields[1], "%.*f/%.*f",
					PREC (args.count), ROUND (args.count),
					PREC (args.goal), ROUND (args.goal));
		} else {
			asprintf (&records[1].fields[0], "%.*f/%.*f",
					PREC (today), ROUND (today),
					PREC (daily_goal), ROUND (daily_goal));
			asprintf (&records[1].fields[1], "%.*f/%.*f",
					PREC (args.count), ROUND (args.count),
					PREC (args.goal), ROUND (args.goal));
		}
	}
	print_table (records, 2, 2);
#undef PREC
#undef ROUND
}

double get_daily_goal (struct args args, double previous_count) {
	double res;
	res = (previous_count - args.goal) / args.n_time_units;
	/* if (args.flags & ARGS_FLAG_BACKWARDS) res = -res; */
	CLAMP (res, 0, args.goal - previous_count);
	return res;
}

int main (int argc, char **argv) {

	struct tm set_to_tm = {0};
	set_to_tm.tm_year = set_to_tm.tm_mon = set_to_tm.tm_mday = -1;

	/* today_midnight = ts_time_with_units_set (now.timestamp, */
	/* 		set_to_tm); */

	struct ts_time now = ts_time_from_timestamp (time (NULL));
	get_args (argc, argv, now);

	double daily_goal = 0;
	struct interval_arr intervals = {0};
	if (!args.tracker_file) {
		daily_goal = get_daily_goal (args, args.start_val);
		print (NAN, daily_goal);
	} else {
		intervals = read_tracker_file (args.tracker_file, &now);

		double previous_count = args.start_val;
		if (!intervals.n) {
			push_interval_arr (&intervals, (struct interval)
					{.date =
					args._this_interval_start,
					.total = args.count, .today = args.count - args.start_val});
		} else {
			if (args.start_time.timestamp > intervals.d[0].date.timestamp) {
				fprintf (stderr, "\
your --start-date is after your tracker file's first record");
			}
			struct interval *latest_interval = intervals.d + intervals.n - 1;

			if (intervals.n == 1) previous_count = args.start_val;
			else previous_count = intervals.d[intervals.n - 2].total;
			if (args._this_interval_start.timestamp >
					(*latest_interval).date.timestamp) {
				push_interval_arr (&intervals, (struct interval) {.date =
						args._this_interval_start, .total = args.count});

			} else
				(*latest_interval).total = args.count;

			(*latest_interval).today = (*latest_interval).total -
				previous_count;
		}
		daily_goal = get_daily_goal (args, previous_count);

		// Even though I've made the flags 1 << 0-types, these if mean
		// they're mutually-exclusive. It's fine.
		if (args.flags & ARGS_FLAG_PRINT_DAILY_PERCENT)
			print_daily_percent (intervals.d[intervals.n - 1].today,
					daily_goal);
		else if (args.flags & ARGS_FLAG_PRINT_TOTAL_PERCENT)
			print_total_percent (args.count, args.goal);
		else if (args.flags & ARGS_FLAG_PRINT_LOG)
			print_log (intervals);
		else
			print (intervals.d[intervals.n - 1].today, daily_goal);

		write_to_tracker_file ((struct path_or_fp) {.path = args.tracker_file,
				.type = POFP_PATH}, intervals);
	}

	exit (0);
}

