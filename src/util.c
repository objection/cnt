#include "errno.h"
#include <stdlib.h>

#define GET_INT \
	do { \
		errno = 0; \
		*res = 0; \
		*res = strtol (str, &end, 0); \
		if (errno || end == str) return 1; \
		return 0; \
	} while (0);

int get_int (int *res, char *str) {
	char *end;
	GET_INT;
}

int get_double (double *res, char *str) {
	char *end;
	errno = 0;
	*res = 0;
	*res = strtod (str, &end);
	if (errno || end == str) return 1;
	return 0;
}

int get_size_t (size_t *res, char *str) {
	char *end;
	GET_INT;
}
