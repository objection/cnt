#include "tracker.h"
#include "../config.h"
#include "util.h"
#include "error.h"
#include <assert.h>
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

DEFINE_ARR (struct interval, interval);

int nlines;

enum kind {
	KIND_NOT_SET,
	KIND_DATE,
	KIND_DIGITS,
	KIND_EOF,
	KIND_NEWLINE,

	// "Provisional" because I'm only checking to see if it's not got
	// digits. I check with ts_time_string.
	KIND_PROVISIONAL_TIME_STRING,
	KIND_BAD_TOKEN,
};

struct token {
	char *s, *e;
	enum kind kind;
} t;

static int skipwhite_check_eof (FILE *fp) {
	int c;
	while (1) {
		c = fgetc (fp);
		// This is weird and crap.
		if (c == EOF) return 1;
		if (!isblank (c) && c != EOF && ftell (fp)) {
			if (ftell (fp)) fseek (fp, -1, SEEK_CUR);
			break;
		}
	}
	return 0;
}

static void get_token (FILE *fp, char *filename) {
	t.s = scratch;
	t.kind = 0;

	if (skipwhite_check_eof (fp)) {
		t.e = t.s;
		t.s = scratch;
		t.kind = KIND_EOF;
		return;
	}

	int c;
	*t.s = c = fgetc (fp);
	switch (*t.s) {
		case '\n':
			t.kind = KIND_NEWLINE;
			break;
		case '0' ... '9':
		case '-': // fall-through
			t.s++;
			// This is really, really lazy parsing.
			while ((*t.s = c = fgetc (fp))) {
				if (c == EOF) break;
				// Time string doesn't let you 19 - 09
				if (isspace (c)) break;
				// This isn't proper parsing. It'd accept 0.000.0.0.0
				if (!isdigit (*t.s) && *t.s != '.')
					t.kind = KIND_PROVISIONAL_TIME_STRING;
				t.s++;
			}
			if (!t.kind) t.kind = KIND_DIGITS;
			*t.s = '\0';
			break;
		default:
			t.kind = KIND_BAD_TOKEN;
			break;
	}
	if (t.s - scratch >= MAX_TOKEN_LEN) {
		errx (1, "%s:%d: token greater than MAX_TOKEN_LEN (%d)",
				filename, nlines + 1, MAX_TOKEN_LEN);
	}
	// I don't understand why this works.
	if (t.kind != KIND_NEWLINE) fseek (fp, -1, SEEK_CUR);
	t.e = t.s;
	t.s = scratch;
}

#define PRINT_TOKEN \
	do { \
		if (t.kind == KIND_NEWLINE) printf ("newline\n"); \
		else printf ("%.*s\n", (int) (t.e - t.s), t.s); \
	} while (0);

struct interval_arr read_tracker_file (char *file, struct ts_time *now) {

	struct interval_arr res = create_interval_arr (INITIAL_DAY_ALLOC);

	FILE *f = fopen (file, "r");
	if (!f) {
		if (errno == ENOENT) {
			// Whatever
			f = fopen (file, "w");
			fclose (f);
			f = fopen (file, "r");
		} else {
			struct stat stat_buf;
			stat (file, &stat_buf);
			if ((stat_buf.st_mode & S_IFMT) != S_IFREG)
				cnt_err ("\"%s\" isn't a regular file", file);
			else cnt_err ("\"%s\" didn't open", file);
		}
	}

	void expected_err (char *expected) {
		errx (1, "%s:%d: expected %s, got \"%s\"",
				file, nlines + 1, expected, t.s);
	}
	struct interval interval;
	while (get_token (f, file), t.kind != KIND_EOF) {

		/* PRINT_TOKEN; */

		if (t.kind == KIND_EOF) break;
		if (t.kind == KIND_NEWLINE) continue;

		if (t.kind != KIND_PROVISIONAL_TIME_STRING)
			expected_err ("time string");

		// This use of (*now).timestamp, etc. I don't think it matters.
		// It was (*today_midnight).timestamp. I can't think why.
		// I might be introducing a bug here, but I need to not need
		// today_midnight now because I can't know it now.
		interval.date = ts_time_from_string (t.s, t.e,
				&(*now).timestamp, &(*now).tm,
				TS_MATCH_ONLY_ISO);

		if (interval.date.timestamp >= (*now).timestamp)
			errx (1, "%s:%d: date is later than now", file, nlines + 1);

		if (ts_errno)
			errx (1, "%s:%d: %s", file, nlines + 1,
					ts_strerror (ts_errno));

		get_token (f, file);
		if (t.kind != KIND_DIGITS) expected_err ("digits");

		/* PRINT_TOKEN; */
		if (get_double (&interval.total, t.s))
			cnt_err ("%s:%d: %s is bad int", file, nlines + 1,
					t.s);

		get_token (f, file);
		if (t.kind != KIND_NEWLINE)
			expected_err ("newline");

		if (res.n == 0) interval.today = interval.total;
		else interval.today = interval.total - res.d[res.n - 1].total;

		push_interval_arr (&res, interval);
		/* PRINT_TOKEN; */
	}

	fclose (f);
	return res;
}

void write_to_tracker_file (struct path_or_fp file,
		struct interval_arr intervals) {

	FILE *fp;
	if (file.type == POFP_PATH) {
		fp = fopen (file.path, "w");
		if (!file.fp) cnt_err ("\"%s\" didn't open", file.path);
	} else
		fp = file.fp;

	for (struct interval *interval = intervals.d;
			interval - intervals.d < intervals.n; interval++) {
		ts_string_from_tm (scratch, MAX_TOKEN_LEN, &(*interval).date.tm,
				"%y-%m-%dT%H:%M:%S");
		fprintf (fp, "%s %f\n", scratch, (*interval).total);
	}

	if (file.type == POFP_PATH) fclose (fp);
}
