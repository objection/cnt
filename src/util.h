
#define CLAMP(val, min, max) \
	do { \
		if (val < min) val = min; \
		else if (val > max) val = max; \
	} while (0);
int get_double (double *res, char *str); 
int get_int (int *res, char *str); 
