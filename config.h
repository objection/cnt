#include "lib/darr/darr.h"
#include "lib/time_string/time_string.h"

// Changing this will have no perceptible effect.
#define INITIAL_DAY_ALLOC 256
// This is the max length of tokens in the tracker file parser. I'll
// leave it as 64. Dates are no more than 32 and the numebers. Even if you
// were counting stars in the galaxy you'd be hard-pressed to use 64 chars.
#define MAX_TOKEN_LEN 64
// What to multiple daily goal by with --lie.
#define LIE_SCALE 1.1
// What to multiple daily goal by with --nice-lie.
#define NICE_LIE_SCALE 0.9
// Change this to eg 2 if you want eg five decimal places.
#define DOUBLE_PRINT_PRECISION 2
#define DEFAULT_ROUND_THRESHOLD 100

extern char scratch[MAX_TOKEN_LEN];
 
