#if 0
gcc -g3 -Wall -o parser_test parser_test.c \
		../src/tracker.c \
		../lib/time_string/time_string.c \
		../lib/darr/darr.c
exit
#endif
#include "../src/tracker.h"
#include "../lib/time_string/time_string.h"

int main (void) {
	struct ts_time now = ts_time_from_timestamp (time (NULL));

	struct day_arr days = read_tracker_file ("parser_test.txt", &now);
}
